using System;
using System.Windows.Forms;
using System.Drawing;

public class CSharpWindow_v02:Form
{
	public static void Main()
	{
		Application.Run(new CSharpWindow_v02());
	}
	public CSharpWindow_v02()
	{
		Width = 800;
		Height = 600;
		BackColor = Color.Black;
		ResizeRedraw = true;
	}
	protected override void OnPaint(PaintEventArgs pea)
	{
		Graphics grfx = pea.Graphics;
		StringFormat strfmt = new StringFormat();
		strfmt.Alignment = StringAlignment.Center;
		strfmt.LineAlignment = StringAlignment.Center;
		grfx.DrawString("Hello World!!!", Font, new SolidBrush(System.Drawing.Color.Green),ClientRectangle, strfmt);
	}
}