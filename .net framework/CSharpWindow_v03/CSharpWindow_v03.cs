using System;
using System.Windows.Forms;
using System.Drawing;

public class CSharpWindow_v02:Form
{
	public static void Main()
	{
		Application.Run(new CSharpWindow_v02());
	}
	public CSharpWindow_v02()
	{
		Width = 800;
		Height = 600;
		BackColor = Color.Black;
		ResizeRedraw = true;
		this.KeyDown += new KeyEventHandler(MyKeyDown);
		this.MouseDown += new MouseEventHandler(MyMouseDown);
	}
	void MyKeyDown(Object sender, KeyEventArgs e)
	{
		MessageBox.Show("Some Key is pressed");
	}
	
	void MyMouseDown(Object sender, MouseEventArgs e)
	{
		MessageBox.Show("Mouse button is pressed");
	}
}