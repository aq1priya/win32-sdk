//Headers
#include <windows.h>
#include <tchar.h>
#pragma comment(lib, "Msimg32.lib")

//Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
DWORD WINAPI MyThreadProcOne(LPVOID);
DWORD WINAPI MyThreadProcTwo(LPVOID);

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	//code
	// initialization of classmembers of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register above class
	RegisterClassEx(&wndclass);

	//Create window
	hwnd = CreateWindow(szAppName,
		TEXT("My Application"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//Message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//code
	static HANDLE hThread1 = NULL;
	static HANDLE hThread2 = NULL;

	switch (iMsg)
	{
	case WM_CREATE:
		hThread1 = CreateThread(NULL,
			0,
			(LPTHREAD_START_ROUTINE)MyThreadProcOne,
			(LPVOID)hwnd,
			0,
			NULL);

		hThread2 = CreateThread(NULL,
			0,
			(LPTHREAD_START_ROUTINE)MyThreadProcTwo,
			(LPVOID)hwnd,
			0,
			NULL);
		
		//error checking
		if (hThread1 == "NULL")
		{
			MessageBox(hwnd, TEXT("thread 1 is failed to create"), TEXT("Thread1 message"), MB_OK);
			DestroyWindow(hwnd);
		}
		if (hThread2 == "NULL")
		{
			MessageBox(hwnd, TEXT("thread 2 is failed to create"), TEXT("Thread2 message"), MB_OK);
			DestroyWindow(hwnd);
		}
		break;

	case WM_LBUTTONDOWN:
		MessageBox(hwnd, TEXT("I am fourth thread"), TEXT("Message"), MB_OK);
		break;

	case WM_DESTROY:
		CloseHandle(hThread1);
		CloseHandle(hThread2);
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

DWORD WINAPI MyThreadProcOne(LPVOID param)
{
	HDC hdc;
	TCHAR str[255];

	hdc = GetDC((HWND)param);
	SetBkColor(hdc, RGB(0, 0, 0));
	SetTextColor(hdc, RGB(0, 255, 0));
	for (int i = 0; i <= 32767; i++)
	{
		wsprintf(str, "Incrementing: %d", i);
		TextOut(hdc, 5, 5, str, lstrlen(str));
	}
	ReleaseDC((HWND)param, hdc);
	return (0);
}

DWORD WINAPI MyThreadProcTwo(LPVOID param)
{
	HDC hdc;
	TCHAR str[255];

	hdc = GetDC((HWND)param);
	SetBkColor(hdc, RGB(0, 0, 0));
	SetTextColor(hdc, RGB(0, 255, 0));
	for (int i = 32767; i >= 0; i--)
	{
		wsprintf(str, "decrementing: %d", i);
		TextOut(hdc, 5, 25, str, lstrlen(str));
	}
	
	ReleaseDC((HWND)param, hdc);
	return (0);
}
