#include <windows.h>
#include "ClassFactoryDllServerwithRegFile.h"
//Class declaration
class CSumSubstract :public ISum, ISubstract
{
private:
	long m_cRef;
public:
	//Constructor
	CSumSubstract(void);
	//Destructor
	~CSumSubstract(void);
	//IUnknown specific method declaration (inherited)
	HRESULT __stdcall QueryInterface(REFIID, void **);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);
	//ISum specific method declaration (inherited)
	HRESULT __stdcall SumOfTwoIntegers(int, int, int *);
	//ISubstract specific method declaration (inherited)
	HRESULT __stdcall SubstractionOfTwoIntegers(int, int, int *);
};

class CSumSubstractClassFactory :public IClassFactory
{
private:
	long m_cRef;
public:
	//constructor method declarations
	CSumSubstractClassFactory(void);
	//destructor 
	~CSumSubstractClassFactory(void);
	//IUnknown specific method declaration(inherited)
	HRESULT __stdcall QueryInterface(REFIID, void **);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);
	//IClassFactory specific method declaration
	HRESULT __stdcall CreateInstance(IUnknown *, REFIID, void **);
	HRESULT __stdcall LockServer(BOOL);
};

//global variable declarations
long glNumberOfActiveComponents = 0;// number of active components
long glNumberOfServerLocks = 0;// Number of locks on this dll

//DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD dwReason, LPVOID Reserved)
{
	//code
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	}
	return (TRUE);
}

//Implementation of CSumSubstract's constructor method
CSumSubstract::CSumSubstract(void)
{
	//code
	m_cRef = 1;//hardcoded initialization to anticipate possible failure of QueryInterface()
	InterlockedIncrement(&glNumberOfActiveComponents); //increment global counter
}

//Implementation of CSumSubstract's destructor method
CSumSubstract::~CSumSubstract(void)
{
	//code
	InterlockedDecrement(&glNumberOfActiveComponents);
}

//Implementation of CSumSubstract's IUnknown methods
HRESULT CSumSubstract::QueryInterface(REFIID riid, void **ppv)
{
	//code
	if (riid == IID_IUnknown)
		*ppv = static_cast<ISum *>(this);
	else if (riid == IID_ISum)
		*ppv = static_cast<ISum *>(this);
	else if (riid == IID_ISubstract)
		*ppv = static_cast<ISubstract *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

ULONG CSumSubstract::AddRef(void)
{
	//code
	InterlockedIncrement(&m_cRef);
	return(S_OK);
}

ULONG CSumSubstract::Release(void)
{
	//code
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(m_cRef);
}

//Implementation of ISum's Methods
HRESULT CSumSubstract::SumOfTwoIntegers(int num1, int num2, int *pSum)
{
	//code
	*pSum = num1 + num2;
	return(S_OK);
}

//Implementation of ISubstract's Methods
HRESULT CSumSubstract::SubstractionOfTwoIntegers(int num1, int num2, int *pSubstract)
{
	//code
	*pSubstract = num1 - num2;
	return(S_OK);
}

//Implementation of CSumSubstractClassFactory's Constructor method
CSumSubstractClassFactory::CSumSubstractClassFactory(void)
{
	m_cRef = 1;
}

//Implementation of CSumSubstractClassFactory's Destructor method
CSumSubstractClassFactory::~CSumSubstractClassFactory(void)
{

}

//Implementation of CSumSubstractClassFactory's IClassFactory's IUnknown's Methods
HRESULT CSumSubstractClassFactory::QueryInterface(REFIID riid, void **ppv)
{
	//code
	if(riid == IID_IUnknown)
		*ppv = static_cast<IClassFactory *>(this);
	else if (riid == IID_IClassFactory)
		*ppv = static_cast<IClassFactory *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

ULONG CSumSubstractClassFactory::AddRef(void)
{
	//code
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

ULONG CSumSubstractClassFactory::Release(void)
{
	//code
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(m_cRef);
}

//Implementation of CSumSubstractClassFactory's IclassFactory's methods
HRESULT CSumSubstractClassFactory::CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv)
{
	//variable declaration
	CSumSubstract *pCSumSubstract = NULL;
	HRESULT hr;
	//code
	if (pUnkOuter != NULL)
		return(CLASS_E_NOAGGREGATION);
	//Create instance of component i.i CSumSubstract class
	pCSumSubstract = new CSumSubstract;
	if (pCSumSubstract == NULL)
		return(E_OUTOFMEMORY);
	//Get the requested interface
	hr = pCSumSubstract->QueryInterface(riid, ppv);
	pCSumSubstract->Release();//anticipate possible faliure of query interface
	return(hr);
}
HRESULT CSumSubstractClassFactory::LockServer(BOOL fLock)
{
	//code
	if (fLock)
		InterlockedIncrement(&glNumberOfServerLocks);
	else
		InterlockedDecrement(&glNumberOfServerLocks);
	return(S_OK);
}

//Implementation of exported functions from this dll
HRESULT __stdcall DllGetClassObject(REFCLSID rclsid, REFIID riid, void **ppv)
{
	//variable declaration
	CSumSubstractClassFactory *pCSumSubstractClassFactory = NULL;
	HRESULT hr;
	//code
	if (rclsid != CLSID_SumSubstract)
		return(CLASS_E_CLASSNOTAVAILABLE);
	//Create class factory
	pCSumSubstractClassFactory = new CSumSubstractClassFactory;
	if (pCSumSubstractClassFactory == NULL)
		return(E_OUTOFMEMORY);
	hr = pCSumSubstractClassFactory->QueryInterface(riid, ppv);
	pCSumSubstractClassFactory->Release();//anticipate possible faliure of query interface()
	return(hr);
}
HRESULT __stdcall DllCanUnloadNow(void)
{
	//code
	if ((glNumberOfActiveComponents == 0) && (glNumberOfServerLocks == 0))
		return(S_OK);
	else
		return(S_FALSE);
}


