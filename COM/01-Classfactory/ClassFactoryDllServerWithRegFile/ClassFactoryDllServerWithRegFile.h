#pragma once
class ISum :public IUnknown
{
public:
	//ISum specific method declarations
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int *) = 0;//pure virtual
};

class ISubstract :public IUnknown
{
public:
	//ISubstract specific method declarations
	virtual HRESULT __stdcall SubstractionOfTwoIntegers(int, int, int *) = 0;//pure virtual
};

//CLSID of SumSubstract Component {69F6937C-2F5E-4A63-8849-20B9D9572538}
const CLSID CLSID_SumSubstract =
{ 0x69f6937c, 0x2f5e, 0x4a63, 0x88, 0x49, 0x20, 0xb9, 0xd9, 0x57, 0x25, 0x38 };
//IID of ISum Interface {3E5FAE96-6627-434B-8BCE-FB3ED3CD543C}
const IID IID_ISum =
{ 0x3e5fae96, 0x6627, 0x434b, 0x8b, 0xce, 0xfb, 0x3e, 0xd3, 0xcd, 0x54, 0x3c };
//IID of ISubstract Interface {36EA84CE-52C8-487D-9C6E-7298E1973585}
const IID IID_ISubstract =
{ 0x36ea84ce, 0x52c8, 0x487d, 0x9c, 0x6e, 0x72, 0x98, 0xe1, 0x97, 0x35, 0x85 };
