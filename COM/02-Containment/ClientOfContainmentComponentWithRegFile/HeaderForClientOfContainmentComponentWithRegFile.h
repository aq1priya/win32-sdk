class ISum :public IUnknown
{
public:
	//ISum specific method declarations
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int *) = 0;
};
class ISubtract :public IUnknown
{
public:
	//ISubtract specific method declarations
	virtual  HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int *) = 0;
};
class IMultiplication :public IUnknown
{
public:
	//IMultiplication specific method declaration
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int *) = 0;//pure virtual
};
class IDivision :public IUnknown
{
public:
	//IDivision specific method declarations
	virtual HRESULT __stdcall DivisionOfTwoIntegers(int, int, int *) = 0; //pure virtual
};
//CLSID of SumSubstract Component {E2699510-0194-496B-B1BA-E752B4A1641B}
const CLSID CLSID_SumSubtract = { 0xe2699510, 0x194, 0x496b, 0xb1, 0xba, 0xe7, 0x52, 0xb4, 0xa1, 0x64, 0x1b };
//IID of ISum interface {15DCE65F-B900-4223-AB24-61F664CB8F33}
const IID IID_ISum = { 0x15dce65f, 0xb900, 0x4223,0xab, 0x24, 0x61, 0xf6, 0x64, 0xcb, 0x8f, 0x33 };
//IID of ISubtract interface {002019B3-4B5D-4833-B942-BCF9D5A8A4DE}
const IID IID_ISubtract = { 0x2019b3, 0x4b5d, 0x4833, 0xb9, 0x42, 0xbc, 0xf9, 0xd5, 0xa8, 0xa4, 0xde };
//IID of multiplication inerface {509351C2-3915-4426-9E5D-C53944D60B41}
const IID IID_IMultiplication = { 0x509351c2, 0x3915, 0x4426, 0x9e, 0x5d, 0xc5, 0x39, 0x44, 0xd6, 0xb, 0x41 };
//IID of division interface {CA0196AD-38DB-406B-825C-3DDDDA82B989}
const IID IID_IDivision = { 0xca0196ad, 0x38db, 0x406b, 0x82, 0x5c, 0x3d, 0xdd, 0xda, 0x82, 0xb9, 0x89 };
