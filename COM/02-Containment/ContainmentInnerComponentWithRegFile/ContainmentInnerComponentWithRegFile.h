class IMultiplication :public IUnknown
{
public:
	//IMultiplication specific method declaration
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int *) = 0;//pure virtual
};
class IDivision :public IUnknown
{
public:
	//IDivision specific method declarations
	virtual HRESULT __stdcall DivisionOfTwoIntegers(int, int, int *) = 0; //pure virtual
};
//CLSID of MultiplicationDivision component {83E5D634-5525-4272-B347-2C0E68652B82}
const CLSID CLSID_MultiplicationDivision = { 0x83e5d634, 0x5525, 0x4272, 0xb3, 0x47, 0x2c, 0xe, 0x68, 0x65, 0x2b, 0x82 };
//IID of multiplication inerface {509351C2-3915-4426-9E5D-C53944D60B41}
const IID IID_IMultiplication = { 0x509351c2, 0x3915, 0x4426, 0x9e, 0x5d, 0xc5, 0x39, 0x44, 0xd6, 0xb, 0x41 };
//IID of division interface {CA0196AD-38DB-406B-825C-3DDDDA82B989}
const IID IID_IDivision = { 0xca0196ad, 0x38db, 0x406b, 0x82, 0x5c, 0x3d, 0xdd, 0xda, 0x82, 0xb9, 0x89 };
