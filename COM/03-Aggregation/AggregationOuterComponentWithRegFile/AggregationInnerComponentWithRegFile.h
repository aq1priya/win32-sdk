class IMultiplication :public IUnknown
{
public:
	//IMultiplication specific method declarations
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int *) = 0;//pure virtual
};
class IDivision:public IUnknown
{
public:
	//IDivision specific method declarations
	virtual HRESULT __stdcall DivisionOfTwoIntegers(int, int, int *) = 0;//pure virtual
};
//CLSID of MultiplicationDivision Component {F0834AF1-4411-49CB-B06F-05BBF581920C}
const CLSID CLISD_MultiplicationDivision={ 0xf0834af1, 0x4411, 0x49cb, 0xb0, 0x6f, 0x5, 0xbb, 0xf5, 0x81, 0x92, 0xc};
//IID IID_IMultiplcation Interface {37BE85E7-0C43-4EC4-ACEA-3FD179D6CAA5}
const IID IID_IMultiplication = { 0x37be85e7, 0xc43, 0x4ec4, 0xac, 0xea, 0x3f, 0xd1, 0x79, 0xd6, 0xca, 0xa5 };
//IID IID_IDivision Interface {9BF39D33-FFF9-4362-AE40-4AEDD6C0BD71}
const IID IID_IDivision = { 0x9bf39d33, 0xfff9, 0x4362, 0xae, 0x40, 0x4a, 0xed, 0xd6, 0xc0, 0xbd, 0x71 };
