#pragma once
class ISum :public IUnknown
{
public:
	//ISum specific method declarations
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int *) = 0;
};
class ISubtract :public IUnknown
{
public:
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int *) = 0;
};

//CLSID of CSumSubtract // {A27D7899-0FD0-4D1B-9072-136C368A9E73}
const CLSID CLSID_CSumSubtract ={ 0xa27d7899, 0xfd0, 0x4d1b, 0x90, 0x72, 0x13, 0x6c, 0x36, 0x8a, 0x9e, 0x73 };
//IID of ISum Interface // {4604B815-41DC-40CF-B7E5-951D6656018C}
const IID IID_ISum = { 0x4604b815, 0x41dc, 0x40cf, 0xb7, 0xe5, 0x95, 0x1d, 0x66, 0x56, 0x1, 0x8c };
//IID of ISubtract Interface// {EBD092F9-3D4F-44C4-8A28-404FF963DCCC}
const  IID IID_ISubtract = { 0xebd092f9, 0x3d4f, 0x44c4, 0x8a, 0x28, 0x40, 0x4f, 0xf9, 0x63, 0xdc, 0xcc };
