#pragma once
class ISum :public IUnknown
{
public:
	//ISum specific method declarations
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int *) = 0;
};
class ISubtract :public IUnknown
{
public:
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int *) = 0;
};
class IMultiplication :public IUnknown
{
public:
	//IMultiplication specific method declarations
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int *) = 0;//pure virtual
};
class IDivision :public IUnknown
{
public:
	//IDivision specific method declarations
	virtual HRESULT __stdcall DivisionOfTwoIntegers(int, int, int *) = 0;//pure virtual
};
//CLSID of CSumSubtract // {A27D7899-0FD0-4D1B-9072-136C368A9E73}
const CLSID CLSID_CSumSubtract = { 0xa27d7899, 0xfd0, 0x4d1b, 0x90, 0x72, 0x13, 0x6c, 0x36, 0x8a, 0x9e, 0x73 };
//IID of ISum Interface // {4604B815-41DC-40CF-B7E5-951D6656018C}
const IID IID_ISum = { 0x4604b815, 0x41dc, 0x40cf, 0xb7, 0xe5, 0x95, 0x1d, 0x66, 0x56, 0x1, 0x8c };
//IID of ISubtract Interface// {EBD092F9-3D4F-44C4-8A28-404FF963DCCC}
const  IID IID_ISubtract = { 0xebd092f9, 0x3d4f, 0x44c4, 0x8a, 0x28, 0x40, 0x4f, 0xf9, 0x63, 0xdc, 0xcc };
//CLSID of MultiplicationDivision Component {F0834AF1-4411-49CB-B06F-05BBF581920C}
const CLSID CLISD_MultiplicationDivision = { 0xf0834af1, 0x4411, 0x49cb, 0xb0, 0x6f, 0x5, 0xbb, 0xf5, 0x81, 0x92, 0xc };
//IID IID_IMultiplcation Interface {37BE85E7-0C43-4EC4-ACEA-3FD179D6CAA5}
const IID IID_IMultiplication = { 0x37be85e7, 0xc43, 0x4ec4, 0xac, 0xea, 0x3f, 0xd1, 0x79, 0xd6, 0xca, 0xa5 };
//IID IID_IDivision Interface {9BF39D33-FFF9-4362-AE40-4AEDD6C0BD71}
const IID IID_IDivision = { 0x9bf39d33, 0xfff9, 0x4362, 0xae, 0x40, 0x4a, 0xed, 0xd6, 0xc0, 0xbd, 0x71 };
