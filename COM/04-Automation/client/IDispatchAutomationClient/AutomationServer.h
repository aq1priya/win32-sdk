#pragma once

class IMyMath :public IDispatch
{
public:
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int *) = 0;//pure virtual
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int *) = 0;//pure virtual
};

//CLSID of MyMath component {B79C6635-AB9C-41EA-BDBD-A6837A96CBE5}
const CLSID CLSID_MyMath = { 0xb79c6635, 0xab9c, 0x41ea, 0xbd, 0xbd, 0xa6, 0x83, 0x7a, 0x96, 0xcb, 0xe5 };

//IID of ISum interface // {A2A69F2D-E7B7-457A-9A47-700C5E79B044}
const IID IID_IMyMath = { 0xa2a69f2d, 0xe7b7, 0x457a, 0x9a, 0x47, 0x70, 0xc, 0x5e, 0x79, 0xb0, 0x44 };
