class IMyMath :public IDispatch
{
public:
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int *) = 0;//pure virtual
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int *) = 0;//pure virtual
};

//CLSID of MyMath component // {63F7D676-2875-47EE-B6BF-1C3FA21273F7}
const CLSID CLSID_MyMath = { 0x63f7d676, 0x2875, 0x47ee, 0xb6, 0xbf, 0x1c, 0x3f, 0xa2, 0x12, 0x73, 0xf7 };

//IID of ISum interface // {786B04B1-8D31-4FB7-8BBB-AF207A5FB2C5}
const IID IID_IMyMath = { 0x786b04b1, 0x8d31, 0x4fb7, 0x8b, 0xbb, 0xaf, 0x20, 0x7a, 0x5f, 0xb2, 0xc5 };
