#include "pch.h"
#include "App.h"
#include "MyPage.h"

using namespace Windows::UI::Xaml; //ApplicationInitializationCallbackParams,ApplicationInitializationCallback
using namespace Windows::UI::Xaml::Controls;//page

void MyCallbackMethod(ApplicationInitializationCallbackParams^ params)
{
	App^ app = ref new App();
}

App::App() //constructor
{
	//no code
}

int main(Array<String^> ^args)
{
	ApplicationInitializationCallback^ callback = ref new ApplicationInitializationCallback(MyCallbackMethod);
	Application::Start(callback);
	return(0);
}

void App::OnLaunched(LaunchActivatedEventArgs^ args)
{
	Page^ page = ref new MyPage();
	Window::Current->Content = page;
	Window::Current->Activate();
}

