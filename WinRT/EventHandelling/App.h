#pragma once
#include "pch.h"
using namespace Platform;
using namespace Windows::UI::Xaml;
using namespace Windows::ApplicationModel::Activation;

ref class App sealed :public Application
{
public:
	App();
	virtual void OnLaunched(LaunchActivatedEventArgs^ args)override;
};
