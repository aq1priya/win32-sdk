#include "pch.h"
#include "MyPage.h"

using namespace Windows::Foundation;//TypedEventHandler
using namespace Windows::UI::Xaml::Media;//FontFamily
using namespace Windows::UI::Text;//fontstyle, fontweights
using namespace Windows::UI::Xaml;//VA, HA
using namespace Windows::UI;//colors

//constructor
MyPage::MyPage()
{
	Window::Current->CoreWindow->KeyDown += ref new TypedEventHandler<CoreWindow^, KeyEventArgs^>(this, &MyPage::OnKeydown);
	
	Grid^ grid = ref new Grid();
	textblock = ref new TextBlock();
	textblock->Text = "Event handelling!!!";
	textblock->FontFamily = ref new Media::FontFamily("Segoe UI");
	textblock->FontStyle = Windows::UI::Text::FontStyle::Oblique;
	textblock->FontWeight = FontWeights::Bold;
	textblock->FontSize = 120;
	textblock->Foreground = ref new SolidColorBrush(Colors::Gold);
	textblock->HorizontalAlignment = Xaml::HorizontalAlignment::Center;
	textblock->VerticalAlignment = Xaml::VerticalAlignment::Center;
	grid->Children->Append(textblock);

	Button^ button = ref new Button();
	button->Content = "Press Button";
	button->Width = 300;
	button->Height = 100;
	button->BorderThickness = 20;
	button->BorderBrush = ref new SolidColorBrush(Colors::Aquamarine);
	button->Foreground = ref new SolidColorBrush(Colors::BlueViolet);
	button->FontFamily = ref new Media::FontFamily("Segoe UI");
	button->FontSize = 25;
	button->FontStyle = Windows::UI::Text::FontStyle::Oblique;
	button->FontWeight = FontWeights::Bold;
	button->HorizontalAlignment = Xaml::HorizontalAlignment::Center;
	button->VerticalAlignment = Xaml::VerticalAlignment::Bottom;
	button->Click += ref new RoutedEventHandler(this, &MyPage::OnClick);
	
	grid->Children->Append(button);
	this->Content = grid;
}

void MyPage::OnKeydown(CoreWindow^ sender, KeyEventArgs^ args)
{
	textblock->Text = "Key is Pressed.";
}

void MyPage::OnClick(Object^ sender, RoutedEventArgs^ args)
{
	textblock->Text = "Mouse button is Clicked.";
}
