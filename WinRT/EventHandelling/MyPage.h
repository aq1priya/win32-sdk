#pragma once
using namespace Windows::UI::Xaml::Controls;//Page, TextBlock
using namespace Windows::UI::Core;//corewindow, KeyEventArgs
using namespace Windows::UI::Xaml;//RoutedEventArgs

ref class MyPage sealed :public Page
{
private:
	TextBlock^ textblock;

public:
	MyPage();
	void OnKeydown(CoreWindow^ sender, KeyEventArgs^ args);
	void OnClick(Object^ sender, RoutedEventArgs^ args);
};
