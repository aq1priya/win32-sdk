//Headers
#include<windows.h>

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// Variable declaration
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("My Application");

	//code
	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register above Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindow(szAppName,
		TEXT("My Application"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//MessageLoop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//code
	HDC hdc;
	RECT rc;
	PAINTSTRUCT ps;
	static HBRUSH hbrush;
	//hbrush = (HBRUSH)BLACK_BRUSH;
	switch (iMsg)
	{
	case WM_CREATE:
		hbrush = CreateSolidBrush(RGB(128, 128, 128));
		InvalidateRect(hwnd, NULL, TRUE);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'R':
			hbrush = CreateSolidBrush(RGB(255, 0, 0));
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 'C':
			hbrush = CreateSolidBrush(RGB(0, 255, 255));
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 'M':
			hbrush = CreateSolidBrush(RGB(255, 0, 255));
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 'Y':
			hbrush = CreateSolidBrush(RGB(255, 255, 0));
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 'W':
			hbrush = CreateSolidBrush(RGB(255, 255, 255));
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 'K':
			hbrush = CreateSolidBrush(RGB(0, 0, 0));
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 'B':
			hbrush = CreateSolidBrush(RGB(0, 0, 255));
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 'G':
			hbrush = CreateSolidBrush(RGB(0, 255, 0));
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		default:
			hbrush = CreateSolidBrush(RGB(128, 128, 128));
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		}
		break;

	case WM_PAINT:
		GetClientRect(hwnd, &rc);
		hdc = BeginPaint(hwnd, &ps);
		SelectObject(hdc, hbrush);
		FillRect(hdc, &rc, hbrush);
		//Rectangle(hdc, 0, 0, rc.right, rc.bottom);
		EndPaint(hwnd, &ps);
		break;

	case WM_DESTROY:
		DeleteObject(hbrush);
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
