// Headers
#include<windows.h>

// Global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// Variable declaration
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("My Application");

	// code
	// Initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// Register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("My Application"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// Message Loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//code
	TCHAR str[255];
	switch (iMsg)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'M':
			wsprintf(str, "key is Pressed");
			MessageBox(hwnd, str, TEXT("Message for WM_KEYDOWN"), MB_OKCANCEL | MB_ICONINFORMATION);
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		wsprintf(str, "Left mouse button is clicked.");
		MessageBox(hwnd, str, TEXT("Message for WM_LBUTTONDOWN"), MB_OK | MB_ICONEXCLAMATION);
		break;
	case WM_MOVE:
		wsprintf(str, "Window is moved");
		MessageBox(hwnd, str, TEXT("Message for WM_MOVE"), MB_RETRYCANCEL | MB_ICONHAND);
		break;
	case WM_SIZE:
		wsprintf(str, TEXT("Window is resized."));
		MessageBox(hwnd, str, TEXT("Message for WM_SIZE"), MB_YESNO | MB_ICONQUESTION);
		break;
	case WM_CREATE:
		wsprintf(str, ("Window is Created."));
		MessageBox(hwnd, str, TEXT("Message for WM_CREATE"), MB_YESNOCANCEL | MB_ICONERROR);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
