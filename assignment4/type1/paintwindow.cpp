//Headers
#include<windows.h>

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// Variable declaration
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("My Application");

	//code
	//initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register above Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindow(szAppName,
		TEXT("My Application"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//MessageLoop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//code
	HDC hdc;
	RECT rc;
	PAINTSTRUCT ps;
	static char KeyPressed;
	static HBRUSH hbrush;
	hbrush = (HBRUSH)1;
	switch (iMsg)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'R':
			KeyPressed = 'r';
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 'C':
			KeyPressed = 'c';
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 'M':
			KeyPressed = 'm';
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 'Y':
			KeyPressed = 'y';
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 'W':
			KeyPressed = 'w';
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 'K':
			KeyPressed = 'k';
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 'B':
			KeyPressed = 'b';
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		case 'G':
			KeyPressed = 'g';
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		default:
			KeyPressed = 'z';
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		}
		break;

	case WM_PAINT:
		GetClientRect(hwnd, &rc);
		hdc = BeginPaint(hwnd, &ps);
		if (KeyPressed == 'r')
			hbrush = CreateSolidBrush(RGB(255, 0, 0));
		else if (KeyPressed == 'g')
			hbrush = CreateSolidBrush(RGB(0, 255, 0));
		else if (KeyPressed == 'b')
			hbrush = CreateSolidBrush(RGB(0, 0, 255));
		else if (KeyPressed == 'c')
			hbrush = CreateSolidBrush(RGB(0, 255, 255));
		else if (KeyPressed == 'm')
			hbrush = CreateSolidBrush(RGB(255, 0, 255));
		else if (KeyPressed == 'y')
			hbrush = CreateSolidBrush(RGB(255, 255, 0));
		else if (KeyPressed == 'k')
			hbrush = CreateSolidBrush(RGB(0, 0, 0));
		else if (KeyPressed == 'w')
			hbrush = CreateSolidBrush(RGB(255, 255, 255));
		else if (KeyPressed == 'z')
			hbrush = CreateSolidBrush(RGB(128, 128, 128));
		else
		{
			hbrush = CreateSolidBrush(RGB(128, 128, 128));
		}
		SelectObject(hdc, hbrush);
		FillRect(hdc, &rc, hbrush);
		//Rectangle(hdc, 0, 0, rc.right, rc.bottom);
		//InvalidateRect(hwnd, NULL, TRUE);
		EndPaint(hwnd, &ps);
		break;

	case WM_DESTROY:
		DeleteObject(hbrush);
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
